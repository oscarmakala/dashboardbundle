<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\User;
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Anto;
use aimgroup\DashboardBundle\Form\AntoType;
use Symfony\Component\HttpFoundation\Response;
use FOS\ElasticaBundle\Repository;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Util\SecureRandom;
use aimgroup\DashboardBundle\Dao\SearchDao;
use aimgroup\DashboardBundle\Entity\SearchResult;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * This ManageAgentsController is used to manage agents in the system.
 *
 * @author Michael Tarimo
 *
 * @Route("admin/agents")
 */
class ManageAgentsController extends AbstractController {

    var $session;
    public function __construct() {
        $this->session = new Session();
    }
    
    
    /**
     *
     * @Route("/", name="admin/agents")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        
        if(!in_array(13, json_decode($this->session->get('user_role_perms')))){
            return $this->redirect($this->generateUrl('admin'));
        }
        
        
        $results = "empty";
//        $elasticaManager = $this->container->get('fos_elastica.manager');
//        $results = $elasticaManager->getRepository('RestApiBundle:User')->search("oscar");

        $searchResult = new SearchResult();

        $dao = new SearchDao($this->container);
        $results = $dao->searchAgent("karanja");
        
//        echo "<pre style='color:#fff'>";
//        print_r($results);
//        echo "</pre>"; 
        
        
//        $cnt = 1;
//        foreach($results as $key => $value){
//            echo "<p style='color:#fff;'>". $cnt . " | ".$value->getFirstName() . " | " . $value->getlastName() . "</p>";
//            $cnt++;
//        }
        
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('RestApiBundle:User')->findAll();
        return array(
            'title' => "Manage Agents",
            'title_descr' => "List, create, delete, activate Agents",
            'agents' => $users,
            'posts' => "",
            'mqttMessageTopic' => $this->container->getParameter("mqtt_notify_msg")
        );
    }

    /**
     * @param Request $request
     * @Route("/list_agents",name="list_agents")
     * @Method({"POST","GET"})
     */
    public function listAgentAction(Request $request) {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();
            $queryAttrib = $request->query->all();
            $queryString = "
SELECT u.id,u.firstName,u.lastName,u.mobileNumber,u.status,u.type as userType,u.username,u.idNumber,u.agentCode,
u.email, CONCAT(h.lastName, ' ', h.firstName) as speragent, u.agent_exclusivity as exclusive,u.number_devices,r.id as region,t.id as territory
                     FROM RestApiBundle:User u
                     LEFT OUTER JOIN u.region r
                     LEFT OUTER JOIN u.territory t
                     LEFT JOIN u.parent_id h 
                     WHERE u.roles LIKE :roles ";
            if (isset($attributes["msisdn"])) {
                $queryString = $queryString . " AND u.mobileNumber like :searchFilter";
            }
            $query = $this->getDoctrine()->getEntityManager()
                    ->createQuery($queryString)
                    ->setParameter("roles", '%ROLE_AGENT%');
            if (isset($attributes["msisdn"])) {
                $query->setParameter("searchFilter", "%" . $attributes["msisdn"] . "%");
            }
            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);
            $agents = $query->getResult();
            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add a new Agent.
     *
     * @Route("/createUser", name="createUser")
     * @Method("POST")
     */
    public function createAction(Request $request) {

//        $logger = $this->container->get("monolog.logger.ereg");
//        /** @var  $loggedIn User */
//        $loggedIn = $this->get('security.token_storage')->getToken()->getUser();

        $resp = new JsonObject();
        $status = false;
        $message = "ERROR SUBMITING REQUEST";
        try {
            $attributes = json_decode($request->getContent(), true);
            if ($attributes) {

                $em = $this->getDoctrine()->getManager();

                $user = new User();
                $user->setFirstName($attributes['firstName']);
                $user->setLastName($attributes['lastName']);
                $user->setMobileNumber(substr($attributes['msisdn'], -9));


                $isExists = $em->getRepository("RestApiBundle:User")->isMobileExists($user->getMobileNumber());

                if ($isExists) {
                    $status = false;
                    $message = "MOBILE NUMBER ALREADY EXITS";
                } else {
                    $user->setUserType($attributes["userType"]);
                    $user->setUsername($user->getMobileNumber());
                    $user->setEnabled(true);
                    $user->setSalt(rand(1000, 9999));
                    $user->setNumberDevices($attributes["number_devices"]);
                    $user->setAgentExclusivity($attributes["agent_exclusivity"]);
                    $user->setDob(new \DateTime($attributes["dob"]));
                    $user->setStatus(true);
                    $user->setRoles(array("ROLE_AGENT"));

                    /** optional fields */
                    if (isset($attributes["agentCode"]))
                        $user->setAgentCode($attributes['agentCode']);
                    if (isset($attributes['email'])) {
                        $user->setEmail($attributes['email']);
                        $user->setEmailCanonical($user->getEmail());
                    }

                    /** end optional fields */
                    $region = $em->getRepository('DashboardBundle:Region')->findOneBy(array("id" => $attributes["region"]));
                    if ($region) {
                        $user->setRegion($region);
                    }

                    $territory = $em->getRepository('DashboardBundle:Territory')->findOneBy(array("id" => $attributes["territory"]));
                    if ($territory) {
                        $user->setTerritory($territory);
                    }
                    $encoder = $this->container->get('security.password_encoder');
                    $encoded = $encoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($encoded);

                    //generate secret and clientid
                    $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
                    $client = $clientManager->createClient();
                    $client->setRedirectUris(array($this->container->getParameter("hostname")));
                    $client->setAllowedGrantTypes(array("authorization_code", "password", "refresh_token", "token", "client_credentials"));
                    $clientManager->updateClient($client);


                    $confirmationCode = rand(10000000, 99999999); //md5($secureRandom->nextBytes(4));
                    $user->setConfirmationToken($confirmationCode);

                    $device = new Device();
                    $device->setUser($user);
                    $device->setMsisdn($user->getMobileNumber());
                    $device->setPrivateKey($client->getSecret());
                    $device->setPublicKey($client->getPublicId());

                    $device->setAccessToken($confirmationCode);
                    $em->persist($device);


                    $em->flush();
                    $status = true;
                    $message = "SUCCESS";
                    //$logger->info(sprintf("%s has created agent with msisdn:%s", $loggedIn->getUsername(), $user->getUsername()));
                }
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add a new Agent.
     *
     * @Route("/updateUser", name="updateUser")
     * @Method("POST")
     */
    public function updateAction(Request $request) {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();

            $em = $this->getDoctrine()->getManager();

            if (isset($attributes["id"])) {
                /** @var  $user User */
                $user = $em->getRepository('RestApiBundle:User')->findOneBy(array("id" => $attributes["id"]));
                if ($user) {
                    $user->setFirstName($attributes['firstName']);
                    $user->setLastName($attributes['lastName']);
                    $user->setMobileNumber(substr($attributes['mobileNumber'], -9));
                    $user->setStatus($attributes['status']);
                    $user->setAgentCode($attributes['agentCode']);
                    $user->setAgentExclusivity($attributes['exclusive']);

                    $em->flush();
                    $resp->setMessage("SUCCESSFULLY");
                } else {
                    $resp->setResult("ERROR");
                    $resp->setMessage("USER NOT FOUND");
                }
            } else {
                $resp->setResult("ERROR");
                $resp->setMessage("INVALID REQUEST,SERVER ERROR");
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * View Reports
     *
     * @Route("/create_access_token/{id}", name="create_access_token")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:create_access_token.html.twig")
     */
    public function CreateAccessTokenAction($id) {

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('RestApiBundle:User')->findAll();

        return array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'mobileNumber' => $id,
            'accessToken' => rand(10000000, 99999999),
            'agents' => $users
        );
    }

    /**
     * Activate an Agent.
     *
     * @Route("/activate", name="activate")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function activateAction(Request $request) {
        
    }

    /**
     * Deactivate an Agent.
     *
     * @Route("/deactivate", name="deactivate")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function deactivateAction(Request $request) {
        
    }

    /**
     * Assign an Agent to a network.
     *
     * @Route("/assign", name="assign")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function assignNetworkAction(Request $request) {
        
    }

    /**
     * Activate Agents using CVS file.
     *
     * @Route("/activate_group", name="activate_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchActivateAction(Request $request) {
        
    }

    /**
     * Deactivate Agents using CVS file.
     *
     * @Route("/deactivate_group", name="deactivate_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchDeactivateAction(Request $request) {
        
    }

    /**
     * Add Agents using CVS file.
     *
     * @Route("/create_group", name="create_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchCreateAction(Request $request) {
        
    }

    /**
     * Assign Agents to a network
     *
     * @Route("/assign_group", name="assign_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchAssignNetworkAction(Request $request) {
        
    }

    /**
     * Approve Agent
     *
     * @Route("/approve", name="approve")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function approveAgentAction(Request $request) {
        
    }

}
