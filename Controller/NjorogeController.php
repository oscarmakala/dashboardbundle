<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Njoroge;
use aimgroup\DashboardBundle\Form\NjorogeType;

/**
 * Njoroge controller.
 *
 * @Route("/njoroge")
 */
class NjorogeController extends Controller
{

    /**
     * Lists all Njoroge entities.
     *
     * @Route("/", name="njoroge")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:Njoroge')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Njoroge entity.
     *
     * @Route("/", name="njoroge_create")
     * @Method("POST")
     * @Template("DashboardBundle:Njoroge:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Njoroge();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('njoroge_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Njoroge entity.
     *
     * @param Njoroge $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Njoroge $entity)
    {
        $form = $this->createForm(new NjorogeType(), $entity, array(
            'action' => $this->generateUrl('njoroge_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Njoroge entity.
     *
     * @Route("/new", name="njoroge_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Njoroge();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Njoroge entity.
     *
     * @Route("/{id}", name="njoroge_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Njoroge')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Njoroge entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Njoroge entity.
     *
     * @Route("/{id}/edit", name="njoroge_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Njoroge')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Njoroge entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Njoroge entity.
    *
    * @param Njoroge $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Njoroge $entity)
    {
        $form = $this->createForm(new NjorogeType(), $entity, array(
            'action' => $this->generateUrl('njoroge_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Njoroge entity.
     *
     * @Route("/{id}", name="njoroge_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Njoroge:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Njoroge')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Njoroge entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('njoroge_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Njoroge entity.
     *
     * @Route("/{id}", name="njoroge_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:Njoroge')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Njoroge entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('njoroge'));
    }

    /**
     * Creates a form to delete a Njoroge entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('njoroge_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
