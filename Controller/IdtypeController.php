<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Form\IdtypeType;

/**
 * Idtype controller.
 *
 * @Route("/idtype")
 */
class IdtypeController extends Controller
{

    /**
     * Lists all Idtype entities.
     *
     * @Route("/", name="idtype")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();
        
        return array(
            'title' => "Id Types",
            'title_descr' => "Id Types in Use",
            'entities' => $entities
        );
    }
    /**
     * Creates a new Idtype entity.
     *
     * @Route("/", name="idtype_create")
     * @Method("POST")
     * @Template("DashboardBundle:Idtype:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Idtype();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('idtype_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Idtype entity.
     *
     * @param Idtype $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Idtype $entity)
    {
        $form = $this->createForm(new IdtypeType(), $entity, array(
            'action' => $this->generateUrl('idtype_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Idtype entity.
     *
     * @Route("/new", name="idtype_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Idtype();
        $form   = $this->createCreateForm($entity);

        return array(
            'title' => "Id Types",
            'title_descr' => "Id Types in Use",
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Idtype entity.
     *
     * @Route("/{id}", name="idtype_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Idtype')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Idtype entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'title' => "Id Types",
            'title_descr' => "Id Types in Use",
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Idtype entity.
     *
     * @Route("/{id}/edit", name="idtype_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Idtype')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Idtype entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'title' => "Edit Id Types",
            'title_descr' => "Edit Id Type",
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Idtype entity.
    *
    * @param Idtype $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Idtype $entity)
    {
        $form = $this->createForm(new IdtypeType(), $entity, array(
            'action' => $this->generateUrl('idtype_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Idtype entity.
     *
     * @Route("/{id}", name="idtype_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Idtype:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Idtype')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Idtype entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('idtype_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Idtype entity.
     *
     * @Route("/{id}", name="idtype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:Idtype')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Idtype entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('idtype'));
    }

    /**
     * Creates a form to delete a Idtype entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('idtype_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
