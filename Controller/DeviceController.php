<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Device controller.
 *
 * @Route("admin/device")
 */
class DeviceController extends AbstractController
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     *
     * $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
     * $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
     * $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
     *
     * @param Request $request
     * @Route("/list_devices",name="list_devices")
     * @Method({"POST","GET"})
     */
    public function listAgentDeviceAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $attributes = $request->query->all();

            $em = $this->getDoctrine()->getEntityManager();
            $emConfig = $em->getConfiguration();

            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

            $queryString = "SELECT d.id as deviceId,d.accessToken,d.imei,DATE(d.createdOn) as createdOn,d.isActive,d.accessToken,d.msisdn,d.status,d.isTiedToDevice FROM RestApiBundle:Device d ";
            if (isset($attributes["userId"])) {
                $queryString = $queryString . " WHERE d.user = :searchFilter";
            }

            $query = $em->createQuery($queryString);
            if (isset($attributes["userId"])) {
                $query->setParameter("searchFilter", $attributes["userId"]);
            }

            $devices = $query->getResult();
            $resp->setRecords($devices);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/updateAction",name="updateAction")
     * @Method({"POST","GET"})
     * @return JsonResponse
     */
    public function updateDeviceAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();
            if (isset($attributes["deviceId"])) {
                $em = $this->getDoctrine()->getEntityManager();
                $device = $em->getRepository('RestApiBundle:Device')->findOneBy(array("id" => $attributes["deviceId"]));
                if ($device) {
                    $device->setStatus($attributes['status']);
                    $device->setIsTiedToDevice($attributes['isTiedToDevice']);
                    $em->flush();
                }

                $resp->setRecord(array(
                    'deviceId'=>$device->getId(),
                    'status'=>$device->getStatus(),
                    'isTiedToDevice'=>$device->getIsTiedToDevice()
                ));
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/accessToken",name="accessToken")
     * @Method({"POST","GET"})
     * @return JsonResponse
     */
    public function accessTokenAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();
            if (isset($attributes["deviceId"])) {
                $em = $this->getDoctrine()->getEntityManager();
                $device = $em->getRepository('RestApiBundle:Device')->findOneBy(array("id" => $attributes["deviceId"]));
                if ($device) {
                    $confirmationCode = rand(10000000, 99999999);
                    $device->setAccessToken($confirmationCode);
                    $resp->setRecord($device);
                    $em->flush();

                    $resp->setRecord(array(
                        'deviceId'=>$device->getId(),
                        'status'=>$device->getStatus(),
                        'isTiedToDevice'=>$device->getAccessToken()
                    ));
                    $resp->setMessage("SUCCESSFULLY UPDATED");
                } else {
                    $resp->setResult("ERROR");
                    $resp->setMessage("USER NOT FOUND");
                }
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/create",name="create")
     * @Method({"POST","GET"})
     * @return JsonResponse
     */
    public function createDeviceAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();
            if (isset($attributes["id"])) {
                $msisdn = substr($attributes["msisdn"], -9);
                $em = $this->getDoctrine()->getEntityManager();
                $devices = $em->getRepository('RestApiBundle:Device')->findBy(array("user" => $attributes["id"]));
                $isMsisdnExists = false;

                $count = 0;
                foreach ($devices as $device) {
                    if ($msisdn == $device->getMsisdn()) {
                        $isMsisdnExists = true;
                        break;
                    }
                    $count++;
                }
                if ($isMsisdnExists) {
                    $resp->setResult("ERROR");
                    $resp->setMessage("MSISDN ALREADY ATTACHED TO DEVICE");
                } else {
                    if ($devices) {
                        /** @var  $user User */
                        $user = $devices[0]->getUser();
                    } else {
                        $user = $em->getRepository("RestApiBundle:User")->findOneBy(array("id" => $attributes["id"]));
                    }
                    if ($count < $user->getNumberDevices()) {
                        //generate secret and clientid
                        $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
                        $client = $clientManager->createClient();
                        $client->setRedirectUris(array($this->container->getParameter("hostname")));
                        $client->setAllowedGrantTypes(array("authorization_code", "password", "refresh_token", "token", "client_credentials"));
                        $clientManager->updateClient($client);

                        $confirmationCode = rand(10000000, 99999999); //md5($secureRandom->nextBytes(4));
                        $user->setConfirmationToken($confirmationCode);

                        $device = new Device();
                        $device->setUser($user);
                        $device->setMsisdn(substr($msisdn, -9));
                        $device->setPrivateKey($client->getSecret());
                        $device->setPublicKey($client->getPublicId());
                        $device->setAccessToken($confirmationCode);
                        $device = $em->getRepository('RestApiBundle:Device')->save($device);
                        $resp->setRecord($device);
                    } else {
                        $resp->setResult("ERROR");
                        $resp->setMessage("YOU CAN NOT ADD ANY OTHER ADVICE,CONTACT YOUR SYSTEM ADMIN");
                    }
                }

            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }


}
