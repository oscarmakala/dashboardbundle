<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Util\JTableNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class AbstractController extends FOSRestController {

    /**
     *
     * @param type $dataObject The data you wish to put in the response object
     * @param type $responseStatus The status of the response
     * @param type $format This can be 'json' or 'xml'. The daufault is 'json'
     * @return JsonResponse
     */
    protected function buildResponse($dataObject, $responseStatus = Response::HTTP_OK, $format = 'json') {
        $response = new Response();

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new JTableNormalizer(),new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $serializedProduct = $serializer->serialize($dataObject, $format);

        return $response->create($serializedProduct, $responseStatus);
    }



}