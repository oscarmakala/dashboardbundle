<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\RestApiBundle\Entity\Role;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Admin;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Entity\ReportsRequests;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\DashboardBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use aimgroup\DashboardBundle\Entity\Faq;
use aimgroup\DashboardBundle\Form\FaqType;
use aimgroup\RestApiBundle\Form\RoleType;
use aimgroup\DashboardBundle\DQL\DateFormatFunction;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Session\Session;
use aimgroup\DashboardBundle\Dao\SearchDao;
use aimgroup\DashboardBundle\Entity\SearchResult;

//use DashboardBundle\Form\FaqType;

/**
 * AdminController controller.
 *
 * @Route("/admin")
 */
class AdminController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all Faq entities.
     *
     * @Route("/", name="admin")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();


        $this->session->set('user_role_perms', json_encode($this->get_department_perms_arrayAction()));

        /*
          //---- Session Games Start

          $session = new Session();
          //        $session->start();
          // set and get session attributes
          $session->set('name', 'Drak');
          $session->get('name');

          // set flash messages
          $session->getFlashBag()->add('notice', 'Profile updated');

          // retrieve messages
          foreach ($session->getFlashBag()->get('notice', array()) as $message) {
          echo '<div class="flash-notice"> ' . $session->get('name') . ' | ' . $message . '</div>';
          }

          $em = $this->getDoctrine()->getManager();

          $entity = $em->getRepository('RestApiBundle:Role')->find($id);
          $entity_perms = $entity->getDeptPerms();

          //---- Session Games End
         */
        //$entities = $em->getRepository('DashboardBundle:Admin')->findAll();

        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }


        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT count(r) as reg_total FROM RestApiBundle:Registration r"
                        . " JOIN r.owner a WHERE 1=1 " . $where
                )->setMaxResults(1);


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');

        $qb = $em->createQueryBuilder('p');
        $qb->select('p')
                ->from('RestApiBundle:Registration', 'p')
                ->where('YEAR(p.dob) = :year')
                ->andWhere('MONTH(p.dob) = :month')
                ->andWhere('DAY(p.dob) = :day');

        $qb->setParameter('year', "1989")
                ->setParameter('month', "09")
                ->setParameter('day', "09");

        $post = $qb->getQuery()->getArrayResult(); //getSingleResult();


        $products = $query->getOneOrNullResult();


        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles




        return array(
            'title' => "View FAQ",
            'title_descr' => "View Frequently Asked Questions",
            'ccount' => $products,
            'akuku' => json_encode($user->getRoles()) . " | " . $this->session->get('user_role_perms'),
            'post' => $post,
            'date_today' => date('Y-m-d'),
            'ccount' => $products
        );
    }

    /**
     *
     * @Route("/change_language/{changeToLocale}", name="change_language")
     * @Method("GET")
     */
    public function changeLanguageAction($changeToLocale) {

//var_dump($request->request);
//$changeToLocale = $request->request->get('loc');
        $this->get('request')->attributes->set('_locale', null);
        $this->get('session')->set('_locale', $changeToLocale);

        $this->get('request')->setLocale($changeToLocale);

//var_dump($changeToLocale);
//var_dump($this->get('request')->getLocale());

        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * Registration Summary
     *
     * @Route("/anto_test", name="registration_summary")
     * @Method("GET")
     * @Template()
     */
    public function registration_summaryAction() {
        $em = $this->getDoctrine()->getManager();

        echo "<p>" . $this->getRequest()->getSchemeAndHttpHost() . $this->getRequest()->getBaseUrl() . "</p>";
        /*
          SELECT count(sreg_dashboard_registrations.id) as count, `state` FROM (`sreg_dashboard_registrations`) GROUP BY `state`
         */

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');

        $result = $em->createQuery("SELECT COUNT(p.id) as ccount, p.state "
                        . "FROM RestApiBundle:Registration p "
                        . "GROUP BY p.state")
                ->getArrayResult();

        $data = array();

//PURGE DATA IS TO BE FETCHED FROM purge table that hosts initially archived data!! nice idea huh ;)
        $purge['approved'] = $purge['pending'] = $purge['declined'] = $purge['total'] = 0;

        $data['filter'] = //$this->input->post('filter'); $filter;
                $data['start'] = "2015-01-01"; //$this->input->post('start'); $start;
        $data['end'] = "2015-01-12"; //$this->input->post('end'); $end;


        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed
        foreach ($result as $key => $value) {

            $total = $total + $value['ccount'];
            if ($value['state'] == 2 || $value['state'] == 4) {
                $approved = $approved + $value['ccount'];
            }
            if ($value['state'] == 0) {
                $pending = $value['ccount'];
            }
            if ($value['state'] == 3) {
                $declined = $value['ccount'];
            }


            if ($value['state'] == 4) {
                $approved_real = $approved_real + $value['ccount'];
            }
            if ($value['state'] == 2) {
                $pending_real = $pending_real + $value['ccount'];
            }
        }

        $data[] = array(
            'total' => number_format($total + $purge['total']),
            'approved' => number_format($approved + $purge['approved']),
            'approved_real' => number_format($approved_real),
            'pending_real' => number_format($pending_real),
            'pending' => number_format($pending + $purge['pending']),
            'rejected' => number_format($declined + $purge['declined'])
        );

        echo json_encode($data);
        exit;
    }

    /**
     * View User Role Departmenrd
     *
     * @Route("/user_role_department", name="user_role_department")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:user_role_department.html.twig")
     */
    public function view_user_role_departmentsAction() {
        if (!in_array(39, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();

        $query = $em->createQueryBuilder()
                ->select("p")
                ->from("RestApiBundle:Role", "p")
                ->getQuery();

        try {
            $results = $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }



        return array(
            'title' => "View User Roles Listing:",
            'title_descr' => "Listing for user permission roles departments ",
            'user_roles' => $results,
                //'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Faq entity.
     *
     * @Route("/edit_role_department/{id}", name="edit_role_department")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:edit_role_department.html.twig")
     */
    public function showedit_role_departmentAction($id) {
        if (!in_array(40, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'dept_id' => $id,
            'title' => "Edit User Role Department Permissions",
            'title_descr' => "Edit User Role Department Permissions",
            'entity_perms' => $entity->getDeptPerms(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing user dept perm
     *
     * @Route("/edit_role_department_action/{id}", name="edit_role_department_action")
     * @Method("POST")
     * @Template("DashboardBundle:Faq:edit.html.twig")
     */
    public function update_edit_role_department_actionAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $attributes = $request->request->all();

        $results = $em->createQuery("UPDATE RestApiBundle:Role u SET u.dept_perms = '" . str_replace('"', "", json_encode($attributes['perms'])) . "' WHERE u.id = " . $id . " ")
                ->getArrayResult();

//return $this->redirect($this->generateUrl('admin/agents'));
        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * hour_graph Summary
     *
     * @Route("/department_perms_json", name="department_perms_json")
     * @Method("GET")
     * @Template()
     */
    public function department_perms_jsonAction() {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();

        $result = $em->createQuery("SELECT p "
                        . " from DashboardBundle:Department_perms p "
                        . " ORDER BY p.parent asc")
                ->getArrayResult(); //getSingleResult();
        $data = array();

        foreach ($result as $key => $value) {
            if ($value['parent'] == 0) {
                $data[$value['id']] = array(
                    'id' => 1,
                    'text' => $value['name'],
                    'children' => array()
                );
            } else {
                $data[$value['parent']]['children'][] = array('text' => $value['name'], 'perm_checked' => 0, 'perm_id' => $value['id']);
            }
        }
        foreach ($data as $ey => $alue) {
            $data_perms[] = $alue;
        }

        echo json_encode($data_perms);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/department_perms_array", name="department_perms_array")
     * @Method("GET")
     * @Template()
     */
    public function get_department_perms_arrayAction() {
        $em = $this->getDoctrine()->getManager();

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles
        $perms_str = str_replace('"', "'", str_replace("]", "", str_replace("[", "", json_encode($user->getRoles()))));

        $emConfig = $em->getConfiguration();
        $result = $em->createQuery("SELECT p "
                        . " from RestApiBundle:Role p "
                        . "WHERE p.role in (" . $perms_str . ")")
                ->getArrayResult(); //getSingleResult();

        $pperms = $unique_perms = array();

        foreach ($result as $key => $value) {
            foreach (json_decode($value['dept_perms']) as $kkey => $vvalue) {
                $pperms[$vvalue] = $vvalue;
            }
        }

        ksort($pperms);
        foreach ($pperms as $kkkkey => $vvvalue) {
            $unique_perms[] = $vvvalue;
        }

        return $unique_perms;
    }

    /**
     * Create New User Role Dept
     *
     * @Route("/newUserDept", name="newUserDept")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:newUserDept.html.twig")
     */
    public function newUserDeptAction() {

        $entity = new Role();
        $form = $this->createCreatedForm($entity);

        return array(
            'title' => "Create New User Role Department",
            'title_descr' => "Create New User Role Department",
            'entity' => $entity,
            'form' => $form->createView(),
        );
        /*
          $entity = new Faq();

          return array(
          'title' => "Create New User Role Department",
          'title_descr' => "Create New User Role Department",
          'entity' => $entity
          );

         */
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/newUserDeptaction", name="newUserDeptaction")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:newUserDept.html.twig")
     */
    public function createnewUserDeptAction(Request $request) {

        $entity = new Role();
        $form = $this->createCreatedForm($entity);
        $form->handleRequest($request);

        if (true || $form->isValid()) { //echo "11111111"; exit;
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_role_department'));
        }

        //echo "222222222"; exit;

        return array(
            'title' => "Create New User Role Department",
            'title_descr' => "Create New User Role Department",
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a form to create a Role entity.
     *
     * @param Role $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreatedForm(Role $entity) {

        $form = $this->createForm(new RoleType(), $entity, array(
            'action' => $this->generateUrl('admin'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Registration Summary
     *
     * @Route("/regbyday_summary/{start_date}/{end_date}", name="regbyday_summary")
     * @Method("GET")
     * @Template()
     */
    public function regbyday_summaryAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        /*
          SELECT DAYNAME(created_date) as day_name, count(id) as day_count
          from sreg_dashboard_registrations
          " . $where . "
          group by day_name
          order by FIELD(day_name, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY');
         */

        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }
        if($start_date != 1){
            $where.= " AND date(p.createdDate) >= '".$start_date."' AND date(p.createdDate) <= '".$end_date."' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DAYNAME', 'DoctrineExtensions\Query\Mysql\DayName');


        $result = $em->createQuery("SELECT DAYNAME(p.createdDate) as day_name, count(p) as day_count "
                        . "from RestApiBundle:Registration p "
                        . " JOIN p.owner a"
                        . " where 1=1 " . $where
                        . " group by day_name")
                ->getArrayResult();


        $response_array = array();
        foreach ($result as $key => $value) {
            $response_array[] = array('day_name' => $value['day_name'], 'day_count' => $value['day_count']);
        }

        echo json_encode($response_array);
        exit;
    }

    /**
     * trendlatest_summary Summary
     *
     * @Route("/trendlatest_summary/{start_date}/{end_date}", name="trendlatest_summary")
     * @Method("GET")
     * @Template()
     */
    public function trendlatest_summaryAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        /*
          SELECT date(created_date) as date, count(sreg_dashboard_registrations.id) as count, `state`
          FROM (`sreg_dashboard_registrations`)
          WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08'
          GROUP BY date(`created_date`), `state`
          ORDER BY date(`created_date`) desc
         */

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }
        if($start_date != 1){
            $where.= " AND date(p.createdDate) >= '".$start_date."' AND date(p.createdDate) <= '".$end_date."' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT p.state as state, DATE(p.createdDate) as ddate, COUNT(p.id) as ccount "
                        . " from RestApiBundle:Registration p "
//. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
                        . " JOIN p.owner a" . $where
                        . " GROUP BY ddate, p.state "
                        . " ORDER BY ddate desc")
                ->getArrayResult(); //getSingleResult();

        $data = $data2 = array();

        foreach ($result as $key => $value) {
            $data[$value['ddate']][$value['state']]['count'] = @$data[$value['ddate']][$value['state']]['count'] + $value['ccount'];
            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];
        }

        foreach ($data as $key4 => $value4) {
            $data2[] = array(
                'date' => $key4,
                'total' => @$value4['total']['count'],
                'approved' => @$value4[4]['count'] + @$value4[2]['count'],
                'pending' => @$value4[0]['count'],
                'declined' => (@$value4[3]['count']) ? @$value4[3]['count'] : "0"
            );
        }

        echo json_encode($data2);
        exit;
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/hourly_summary/{start_date}/{end_date}", name="hourly_summary")
     * @Method("GET")
     * @Template()
     */
    public function hourly_summaryAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT p.state as state, HOUR(p.createdDate) as hhour, COUNT(p.id) as ccount "
                        . " from RestApiBundle:Registration p "
//. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
                        . " JOIN p.owner a" . $where
                        . " GROUP BY hhour "
                        . " ORDER BY hhour desc")
                ->getArrayResult(); //getSingleResult();

        $data = array();

        for ($i = 0; $i <= 23; $i++) {
            $data[$i] = 0;
        }

        foreach ($result as $key => $value) {
            //echo "<pre>"; print_r($value); echo "</pre>";

            $data[$value['hhour']] = $value['ccount'];
        }


        echo json_encode($data);
        exit;
    }

    /**
     * graphreport_region Summary
     *
     * @Route("/graphreport_region/{start_date}/{end_date}/{territory}", name="graphreport_region")
     * @Method("GET")
     * @Template()
     */
    public function graphreport_regionAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        //since territory [town names ] isnt left joined plus saving time lets pick code from array instead :(
        //first get territories iterate to form array then pick!
        $result = $em->createQuery("SELECT t from DashboardBundle:Territory t ")->getArrayResult(); //getSingleResult();

        foreach ($result as $key => $value) {
            $regionskey[$value['name']] = $value['id'];
            $regionsName[$value['id']] = $value['name'];
        }

        $regionsName['EntireCountry'] = $regionskey['EntireCountry'] = "EntireCountry";
        $regionskey['all_regions'] = "EntireCountry";

//        echo "<p>regionsName Holding Array: <pre>";
//        print_r($regionsName);
//        echo "</pre></p>";

        $postedRegions = explode(",", $territory);
        foreach ($postedRegions as $xkey => $xval) {
            $regionscode[] = $regionskey[strtolower($xval)];
        }

        $regionssearch = "";
        foreach ($regionscode as $tkey => $tval) {
            $regionssearch.=$tval . ",";
        }
        $regionssearch = rtrim($regionssearch, ",");


        $group_by = $territory_field = "";
        $territory_field = ", 'EntireCountry' AS  tterritory";
        $territory_list = "'" . str_replace(",", "','", $territory) . "'";
        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";
        if ($territory != "all_regions") {
            $where.= " AND p.territory in (" . $regionssearch . ") ";
            $group_by = ", tterritory";
            $territory_field = ", p.territory as tterritory";
        }

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        //* DESIRED: SELECT count(*) as ccount, gender, DATE_FORMAT(createdDate, '%Y%m') as mmonth, territory, EntireCountry' AS  tterritory FROM `registration` group by mmonth, gender order by id desc */
        $result = $em->createQuery("SELECT DATE_FORMAT(p.createdDate, '%Y%m') as mmonth, COUNT(p.id) as ccount, p.gender " . $territory_field
                        . " from RestApiBundle:Registration p "
                        // . " JOIN p.owner a "
                        // . " JOIN p.region b "
                        . $where
                        . " GROUP BY mmonth, p.gender" . $group_by
                        . " ORDER BY mmonth desc")
                ->getArrayResult(); //getSingleResult();
        //echo $em->getSql();

        $data_gender = array();
        $data2[0][0][0] = "[]";
        $data2[0][0][1] = "[]";
        $data2[1]['male'] = 0;
        $data2[1]['female'] = 0;
        $data2[2] = array();


        //get all months and all dates
        $month_list = $territory_list = array();
        $cnt = 1;
        foreach ($result as $key => $value) {

            $antoo2[] = $regionsName[$value['tterritory']] . "|" . $value['tterritory'];
//            echo "<p> Results from DB: " . $cnt . ". <pre>";
//            print_r($value);
//            echo "</pre></p><p> --- -- -  -- </p><br />";
            if (!in_array($value['mmonth'], $month_list)) {
                $month_list[] = $value['mmonth'];
            }
            $territory_list[$regionsName[$value['tterritory']]] = $regionsName[$value['tterritory']];
            $cnt++;
        }

//        echo ":::<p>TerritoryList: <pre>";
//        print_r($territory_list);
//        echo "</pre></p><p> --- -- -  -- </p><br />";

        //create interesting empty array for territory Gender and Registrations
        foreach ($territory_list as $tkey => $tval) {

            foreach ($month_list as $mkey => $mval) {
                $territory_data['data'][$tval][$mval] = 0;
                $territory_data['gender'][$tval]['male'] = 0;
                $territory_data['gender'][$tval]['female'] = 0;
            }
        }

//        echo "<p>Empty Array: <pre>";
//        print_r($territory_data);
//        echo "</pre></p>";


        //Abracadabra!! let the magic happen!! 
        foreach ($result as $kkey => $vvalue) {

            $antoo[] = $regionsName[$vvalue['tterritory']] . "|" . $vvalue['tterritory'];
            $territory_data['data'][$regionsName[$vvalue['tterritory']]][$vvalue['mmonth']] = $territory_data['data'][$regionsName[$vvalue['tterritory']]][$vvalue['mmonth']] + $vvalue['ccount'];

            if (strtolower($vvalue['gender']) == 'male' || strtolower($vvalue['gender']) == 'm') {
                $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['male'] = $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['male'] + $vvalue['ccount'];
            } else {
                $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['female'] = $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['female'] + $vvalue['ccount'];
            }
        }

//        echo "<p>1: ".json_encode($antoo2)."</p>";
//        echo "<p>2: ".json_encode($antoo)."</p>";
//        echo " : <p>Filled Array: <pre>";
//        print_r($territory_data);
//        echo "</pre></p>";

        $territory_data['month_list'] = $month_list;
        $territory_data['ccount_elements'] = count(@$territory_data['data']);


        /*
          ++++++++ ++ + ++ ++
          Array
          (
          [data] => Array
          (
          [arusha] => Array
          (
          [201601] => 0
          [201512] => 0
          [201511] => 0
          [201509] => 0
          [201508] => 0
          )
          [iringa] => Array
          (
          [201601] => 0
          [201512] => 0
          [201511] => 0
          [201509] => 0
          [201508] => 0
          )

          )
          [gender] => Array
          (
          [arusha] => Array
          (
          [male] => 0
          [female] => 0
          )
          [iringa] => Array
          (
          [male] => 0
          [female] => 0
          )
          )
          )
          ++++++++ ++ + ++ ++
          Array
          (
          [mmonth] => 201512
          [ccount] => 1
          [gender] => male
          [tterritory] => arusha
          )
         */
//
//        echo "<p> ++++++++ ++ + ++ ++<pre>";
//        print_r($territory_data);
//        echo "</pre></p><p> ++++++++ ++ + ++ ++</p><br />";

        echo json_encode($territory_data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/hour_graph/{start_date}/{end_date}/{territory}", name="hour_graph")
     * @Method("GET")
     * @Template()
     */
    public function hour_graphAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        
        //since territory [town names ] isnt left joined plus saving time lets pick code from array instead :(
        //first get territories iterate to form array then pick!
        $result = $em->createQuery("SELECT t from DashboardBundle:Territory t ")->getArrayResult(); //getSingleResult();

        foreach ($result as $key => $value) {
            $regionskey[$value['name']] = $value['id'];
            $regionsName[$value['id']] = $value['name'];
        }

        $regionsName['EntireCountry'] = $regionskey['EntireCountry'] = "EntireCountry";
        $regionskey['all_regions'] = "EntireCountry";

//        echo "<p>regionsName Holding Array: <pre>";
//        print_r($regionsName);
//        echo "</pre></p>";

        $postedRegions = explode(",", $territory);
        foreach ($postedRegions as $xkey => $xval) {
            $regionscode[] = $regionskey[strtolower($xval)];
        }

        $regionssearch = "";
        foreach ($regionscode as $tkey => $tval) {
            $regionssearch.=$tval . ",";
        }
        $regionssearch = rtrim($regionssearch, ",");
        
        
        
        $territory = strtolower($territory);
        $group_by = $territory_field = "";
        $territory_field = ", 'EntireCountry' AS  tterritory";
        //echo '[[["[]","[]"]],{"male":0,"female":0},[]]'; exit;
        $territory_list = "'" . str_replace(",", "','", $territory) . "'";
        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";
        if ($territory != "all_regions") {
            $where.= " AND p.territory in (" . $regionssearch . ") ";
            $group_by = ", tterritory";
            $territory_field = ", p.territory as tterritory";
        } else {
            $territory = "EntireCountry";
        }

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, HOUR(p.createdDate) as hhour " . $territory_field
                        . " from RestApiBundle:Registration p "
                        . " JOIN p.owner a" . $where
                        . " GROUP BY hhour "
                        . " ORDER BY hhour desc")
                ->getArrayResult(); //getSingleResult(); //getSql

        $data = array();

        $string_hour = "[";
        $string_data = "[";
        $cnt = 1;
        foreach (explode(",", $territory) as $key => $value) {

            for ($i = 0; $i <= 23; $i++) {
                $data[$value][$i] = 0;
            }
            $cnt++;
        }
        
//        echo "<p>BEFORE: <pre>";
//        print_r($data);
//        echo "</pre></p><p> --- -- -  -- </p><br />";


        //get all months and all dates
        $month_list = $territory_list = array();
        foreach ($result as $key => $value) {
            $data[$regionsName[$value['tterritory']]][$value['hhour']] = $value['ccount'];
        }

//        echo "<p>POPULATED:<pre>";
//        print_r($data);
//        echo "</pre></p><p> --- -- -  -- </p><br />";

        echo json_encode($data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentregistrations/{start_date}/{end_date}/{territory}", name="agentregistrations")
     * @Method("GET")
     * @Template()
     */
    public function agentregistrations_mapAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);

        //echo '[[["[]","[]"]],{"male":0,"female":0},[]]'; exit;
        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, p.territory as territory "
                        . " from RestApiBundle:Registration p " . $where
                        . " GROUP BY territory ")
                ->getArrayResult(); //getSingleResult(); //getSql

        /*
          {
          "code": "GE",
          "z": 362
          }
         */

        $regionskey = array(
            "mwanza" => "MW",
            "kagera" => "KR",
            "pwani" => "PW",
            "morogoro" => "MO",
            "njombe" => "NJ",
            "zanzibar" => "ZS",
            "zanzibar" => "ZW",
            "zanzibar" => "ZN",
            "kigoma" => "KM",
            "mtwara" => "MT",
            "ruvuma" => "RV",
            "pemba" => "PN",
            "pemba" => "PS",
            "singida" => "SD",
            "shinyanga" => "SH",
            "arusha" => "AS",
            "manyara" => "MY",
            "mara" => "MA",
            "simiyu" => "SI",
            "mbeya" => "MB",
            "rukwa" => "RK",
            "dar-es-salaam" => "DS",
            "dodoma" => "DO",
            "tabora" => "TB",
            "lindi" => "LI",
            "Geita" => "GE",
            "kilimanjaro" => "KL",
            "tanga" => "TN",
            "kahama" => "KA",
            "iringa" => "IR"
        );

        $data = array();
        foreach ($regionskey as $key => $value) {
            $data[$key] = array("code" => $value, "z" => 0);
        }

        foreach ($result as $kkey => $vval) {
            $data[$vval['territory']]['z'] = $vval['ccount'];
        }


//        echo "<p><pre>";
//        print_r($data);
//        echo "</pre></p><p> --- --- --</p><br />";

        echo json_encode($data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentlocation_map/{start_date}/{end_date}/{territory}", name="agentlocation_map")
     * @Method("GET")
     * @Template()
     */
    public function agentlocation_mapAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);

        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, p.status, a.name as tterritory "
                        . " from RestApiBundle:User p "
                        . " JOIN p.territory a"
                        . " GROUP BY tterritory ")
                ->getArrayResult(); //getSingleResult(); //getSql

        $regionskey = array(
            "mwanza" => "MW",
            "kagera" => "KR",
            "pwani" => "PW",
            "morogoro" => "MO",
            "njombe" => "NJ",
            "zanzibar" => "ZS",
            "zanzibar" => "ZW",
            "zanzibar" => "ZN",
            "kigoma" => "KM",
            "mtwara" => "MT",
            "ruvuma" => "RV",
            "pemba" => "PN",
            "pemba" => "PS",
            "singida" => "SD",
            "shinyanga" => "SH",
            "arusha" => "AS",
            "manyara" => "MY",
            "mara" => "MA",
            "simiyu" => "SI",
            "mbeya" => "MB",
            "rukwa" => "RK",
            "dar-es-salaam" => "DS",
            "dodoma" => "DO",
            "tabora" => "TB",
            "lindi" => "LI",
            "Geita" => "GE",
            "kilimanjaro" => "KL",
            "tanga" => "TN",
            "kahama" => "KA",
            "iringa" => "IR"
        );

        $data = array();
        foreach ($regionskey as $key => $value) {
            $data[$key] = array("code" => $value, "z" => 0);
        }

        foreach ($result as $kkey => $vval) {
            $data[$vval['tterritory']]['z'] = $vval['ccount'];
        }

        echo json_encode($data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentactiveinactive_map/{start_date}/{end_date}/{territory}", name="agentactiveinactive_map")
     * @Method("GET")
     * @Template()
     */
    public function agentactiveinactive_mapAction($start_date, $end_date, $territory) {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);
        $group_by = $territory_field = "";
        $territory_field = ", 'EntireCountry' AS  tterritory";

        $territory_list = "'" . str_replace(",", "','", $territory) . "'";
        $where = " WHERE 1=1 ";

        if ($territory != "all_regions") {
            $where.= " AND a.name in (" . $territory_list . ") ";
            $group_by = ", tterritory";
            $territory_field = ", a.name as tterritory";
        } else {
            $territory = "EntireCountry";
        }



        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, p.status " . $territory_field
                        . " from RestApiBundle:User p "
                        . " JOIN p.territory a" . $where
                        . " GROUP BY tterritory, p.status ")
                ->getArrayResult(); //getSingleResult(); //getSql

        $territory_array = explode(",", $territory);

        $data['ccount_elements'] = count($territory_array);
        foreach ($territory_array as $keyy => $vall) {
            $data['data'][$vall] = array("inactive" => 0, "active" => 0);
        }

        foreach ($result as $kkey => $vval) {

            $sstatus = ($vval['status'] == 1 ? "active" : "inactive");
            $data['data'][$vval['tterritory']][$sstatus] = $vval['ccount'];
            $data['data'][$vval['tterritory']][$sstatus] = $vval['ccount'];
        }

        echo json_encode($data);
        exit;
    }

    /**
     * graphdate_range Summary
     *
     * @Route("/graphdate_range", name="graphdate_range")
     * @Method("GET")
     * @Template()
     */
    public function graphdate_rangeAction() {
        $em = $this->getDoctrine()->getManager();

        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

//$emConfig->addCustomStringFunction($name, 'aimgroup\DashboardBundle\DQL');
        $emConfig->addCustomNumericFunction('FLOOR', 'aimgroup\DashboardBundle\DQL\MysqlFloor');
//$emConfig->addCustomDatetimeFunction($name, 'aimgroup\DashboardBundle\DQL');



        $result = $em->createQuery("SELECT COUNT(p) as ccount, CASE WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) <= 10 THEN '0-10'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >= 10 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <= 20 THEN '11-20'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=21 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=30 THEN '21-30'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=31 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=40 THEN '31-40'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=41 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <= 50 THEN '31-40'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=51 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=60 THEN '51-60'
		
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=61 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=70 THEN '61-70'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=71 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=80 THEN '71-80'
        WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=81 THEN '81+'
	  END AS ageband  from RestApiBundle:Registration p GROUP BY ageband  ORDER BY ageband desc")
                ->getArrayResult(); //getSingleResult();

        echo json_encode($result);
        exit;
    }

    /**
     * @param Request $request
     * @Route("/passwordChange", name="passwordChange")
     * @Method("POST")
     */
    public function passwordChangeAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        $message = "REQUEST FAILED";
        $attributes = json_decode($request->getContent(), true);




        $requestString = $request->getContent();
        // $this->get("api.helper")->log("saveUserPasswordAction", $requestString);
        try {
            $json = json_decode($requestString, true);

            $username = "admin"; //$json['username'];
            $password = "akuku"; //$json["password"];
            $em = $this->getDoctrine()->getManager();
            /** @var  $device Device */
            $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
            $user = $queryBuilder->select('u')
                            ->from('RestApiBundle:User', 'u')
                            ->where('u.username = :username')
                            ->setParameter('username', $username)
                            ->getQuery()->getOneOrNullResult();


            if ($user) {
                //if device exists update user
                /** @var  $user User */
                $user->setPassword($password);
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                $em->flush();
                $status = true;
                $message = "SUCCESS";
            } else {
                echo "patee user";
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        //$this->get("api.helper")->log("saveUserPasswordAction", array($resp->getMessage(), $username), true);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * top_agents Summary
     *
     * @Route("/top_agents/{start_date}/{end_date}", name="top_agents")
     * @Method("GET")
     * @Template()
     */
    public function top_agentsAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        /*
         * SELECT `phone`, `users`.`first_name` as first_name, `users`.`last_name` as last_name, count(sreg_dashboard_registrations.id) as count
         * FROM (`sreg_dashboard_registrations`) 
         * LEFT JOIN `users` ON `sreg_dashboard_registrations`.`created_by` = `users`.`id` 
         * GROUP BY `sreg_dashboard_registrations`.`created_by` 
         * ORDER BY `count` desc 
         * LIMIT 10
         */

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND u.parent_id = " . $user->getId();
        }
        if($start_date != 1){
            $where.= " AND date(r.createdDate) >= '".$start_date."' AND date(r.createdDate) <= '".$end_date."' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "
        $query = $em->createQueryBuilder()
                ->select("u.username, CONCAT(u.firstName , ' ', u.lastName) as agent_names, COUNT(r.id) as registrations_count"
                        . " from RestApiBundle:Registration r"
                        . " JOIN r.owner u " . $where
                        . " GROUP BY r.owner ")
                ->getQuery();

        $data2 = $query->getResult();

        $data = array();
        foreach ($data2 as $key => $value) {
            $data[] = array('name' => $value['agent_names'], 'count' => $value['registrations_count']);
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Users
     *
     * @Route("/view_agents", name="view_agents")
     * @Method("GET")
     * @Template()
     */
    public function view_agentsAction() {
        $em = $this->getDoctrine()->getManager();

//$entities = $em->getRepository('DashboardBundle:Admin')->findAll();
        $entities_user = $em->getRepository('DashboardBundle:User')->findAll();

        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            'entities_user' => $entities_user,
                //'entities' => $entities,
        );
    }

    /**
     * View Reports
     *
     * @Route("/reports", name="reports")
     * @Method("GET")
     * @Template()
     */
    public function reportsAction() {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        /*
          $response = new StreamedResponse();
          $response->setCallback(function() {

          $em = $this->getDoctrine()->getManager();
          $handle = fopen('php://output', 'w+');

          // Add the header of the CSV file
          fputcsv($handle, array('Name', 'Surname', 'Age', 'Sex'), ',');
          // Query data from database
          $results = $em->createQuery("SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ")
          ->getArrayResult();
          // Add the data queried from database
          foreach ($results as $key => $row) {
          fputcsv(
          $handle, // The file pointer
          array($row['firstName'], $row['lastName'], $row['region'], $row['territory']), // The fields
          ',' // The delimiter
          );
          }

          fclose($handle);
          });

          $response->setStatusCode(200);
          $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
          $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

          return $response;

         */

        /*
          //--....... Sending Email
          // send random password to the email
          //modify mailer settings
          $transport = \Swift_SmtpTransport::newInstance($this->container->getParameter('mailer_host'), $this->container->getParameter('mailer_port'), 'ssl')
          ->setUsername($this->container->getParameter('mailer_user'))
          ->setPassword($this->container->getParameter('mailer_password'));

          // send random password to the email
          $message = \Swift_Message::newInstance()
          ->setSubject('E-reg 2.0 Report | 2015-11-20')
          ->setFrom($this->container->getParameter('system_email_address'))
          ->setTo("mutisya06@gmail.com")
          ->setBody(
          $this->renderView(
          // app/Resources/views/Emails/registration.html.twig
          'emails/incoming_report.html.twig', array('name' => "mutisyaaaaa",)
          ), 'text/html'
          );

          try {
          $mailer = \Swift_Mailer::newInstance($transport);
          $mailer->send($message);
          } catch (\Exception $exception) {
          #var_dump($exception->getMessage());
          }

          //--.......
         */
        return array(
            'title' => "Generate Reports",
            'title_descr' => "View Reports for",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/reportspost", name="reportspost")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function reportspostAction(Request $request) {

        global $post;
        $post = $request;

        $response = new StreamedResponse();
        $response->setCallback(function () {

            global $post;

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

            $where_clause = " 1=1";
            if ($post->request->get('region')) {
//$where_clause.= " AND p.region = " . $post->request->get('region');
            }
            if ($post->request->get('territory')) {
//$where_clause.= " AND p.territory = " . $post->request->get('territory');
            }
            if ($post->request->get('from_date')) {
//$where_clause.= " AND date(p.createdDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
            }
            if ($post->request->get('to_date')) {
//$where_clause.= " AND date(p.createdDate) >= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
            }
            if ($post->request->get('customer_msisdn')) {//----customer msisdn
//$where_clause.= " AND p.msisdn = " . $post->request->get('customer_msisdn');
            }
            if ($post->request->get('agent_msisdn')) {//----customer msisdn
                $where_clause.= " AND u.username = '" . $post->request->get('agent_msisdn') . "'";
            }

            $handle = fopen('php://output', 'w+');

            switch ($post->request->get('report_type')) {
                case "agent_registration":

// Add the header of the CSV file
                    fputcsv($handle, array('Customer Number', 'Customer Name', 'ID Number', 'ID Type', 'Registrar MSISDN', 'Registrar Name', 'Region Name', 'Territory Name', 'Created Date', 'Registration Type'), ',');
// Query data from database 
                    $query = $em->createQueryBuilder()
                            ->select("p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name, p.region, p.territory, date(p.createdDate), p.state ")
                            ->from("RestApiBundle:Registration", "p")
                            ->leftJoin('p.owner', 'u')
                            ->where($where_clause)
                            ->getQuery();

                    break;
                case "id_documents_eport":

// Add the header of the CSV file
                    fputcsv($handle, array('ID Type', 'Number of Usage'), ',');
                    $query = $em->createQueryBuilder()
                            ->select("p.identificationType, COUNT(p)")
                            ->from("RestApiBundle:Registration", "p")
                            ->leftJoin('p.owner', 'u')
                            ->groupBy('p.identificationType')
                            ->where($where_clause)
                            ->getQuery();

                    break;
                case "registration_region_report":

// Add the header of the CSV file
                    fputcsv($handle, array('Region Name', 'Territory Name', 'Registrations Done'), ',');
                    $query = $em->createQueryBuilder()
                            ->select("p.region, p.territory, COUNT(p)")
                            ->from("RestApiBundle:Registration", "p")
                            ->groupBy('p.region')
                            ->groupBy('p.territory')
                            ->where($where_clause)
                            ->getQuery();

                    break;
                case "registrations_gender_report":

// Add the header of the CSV file
                    fputcsv($handle, array('Gender', 'Registrations Done'), ',');
                    $query = $em->createQueryBuilder()
                            ->select("p.gender, COUNT(p)")
                            ->from("RestApiBundle:Registration", "p")
                            ->leftJoin('p.owner', 'u')
                            ->groupBy('p.gender')
                            ->where($where_clause)
                            ->getQuery();
                    break;
                case "active_inactive_agents":
//code to be executed if n = label3;
                    break;
                default:
//code to be executed if n is different from all labels;
                    $results = $em->createQuery("SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ")
                            ->getArrayResult();
            }

            try {
                $results = $query->getArrayResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                return null;
            }

//$results = $em->createQuery("SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ")
//            ->getArrayResult();
// Add the data queried from database
            foreach ($results as $key => $row) {
                fputcsv(
                        $handle, // The file pointer
                        $row, // The fields
                        ',' // The delimiter
                );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');


        return $response;

        /* return array(
          'title' => "Generate Reports",
          'title_descr' => "View Reports for",
          'message' => json_encode($request->getContent()) //"Report Request made successfully.. Report will be sent to email once generated",
          ); */
//return $this->redirect($this->generateUrl('reports'));
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/registrations_filter", name="registrations_filter")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:registrations.html.twig")
     */
    public function registrations_filterAction(Request $request) {

        $post = $request;
        $post->request->get('customer_msisdn');



        $results = "empty";
        $searchResult = new SearchResult();

        $dao = new SearchDao($this->container);
        $results = $dao->searchAgent($post->request->get('first_name'));

//        $cnt = 1;
//        foreach ($results as $key => $value) {
//            echo "<p style='color:#fff;'>" . $cnt . " | " . $value->getFirstName() . " | " . $value->getlastName() . "</p>";
//            $cnt++;
//        }

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RestApiBundle:Registration')->findAll();

        return array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report | All registrations",
            'registrations' => $results,
            'message' => json_encode($request->getContent()) //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }

    /**
     * View Reports
     *
     * @Route("/registration_info/{id}", name="registration_info")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registration_info.html.twig")
     */
    public function registrationInfoAction($id) {
        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }

        return array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'entity' => $entity,
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/agent_edit/{id}", name="agent_edit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_edit.html.twig")
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $query = $em->createQuery("SELECT d FROM RestApiBundle:Device d WHERE d.user = 1")//'".$entity->getId()."'")
                ->getResult();


        $editForm = $this->createEditForm($entity);

        return array(
            'title' => "Create New User",
            'title_descr' => "Create New Agent",
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'devices' => $query,
        );
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity) {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('agent_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('agent_deactivate', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Deactivate'))
                        ->getForm()
        ;
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="agent_deactivate")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $results = $em->createQuery("UPDATE RestApiBundle:User u SET u.enabled = 0 WHERE u.id = " . $id . " ")
                    ->getArrayResult();

//$em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin/agents'));
    }

    /**
     * Edits an existing Faq entity.
     *
     * @Route("/agent_edit/{id}", name="agent_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Admin:agent_edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('agent_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * View Reports
     *
     * @Route("/registration_images/{id}", name="registration_images")
     * @Method("GET")
     *
     */
    public function registrationImagesAction($id) {
        $resp = new JsonObject();
        $status = false;
        $message = "";
        try {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:RegImages')->findBy(array("registration" => $id));
            if ($entity) {
                $resp->setItem($entity);
                $status = true;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);


//        $entity = $em->getRepository('RestApiBundle:RegImages')->findBy(
//            array("registration"=>$id)
//        );
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Registration entity.');
//        }
//        var_dump($entity);
    }

    /**
     * Finds and displays a Faq entity.
     *
     * @Route("/{id}", name="faq_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Faq')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="user_create")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:useragent_new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('view_agents', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity) {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/useragent_new", name="useragent_new")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:useragent_new.html.twig")
     */
    public function newAction() {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * .
     *
     * @Route("/user_account", name="user_account")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:my_account.html.twig")
     */
    public function userAccountAction(Request $request) {
        $data = array();
        return array(
            'title' => "My Account",
            'title_descr' => "List, create, delete, activate System Admins",
            'userInfo' => $data
        );
    }

}
