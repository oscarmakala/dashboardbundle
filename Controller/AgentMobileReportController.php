<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class AgentMobileReportController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("/mreport")
 *
 */
class AgentMobileReportController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/agent/{id}", name="magent")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     */
    public function agentdetailsAction($id)
    {
        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");
        $em = $this->get("doctrine")->getManager();
        try {
            $result = $em->getRepository("RestApiBundle:Registration")->createQueryBuilder("r")
                ->select("count(r.id) as ccount,r.state")
                ->where("r.agentMsisdn = :agentMsisdn")
                ->setParameter("agentMsisdn", $id)
                ->groupBy("r.state")
                ->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            $result = null;
        }
        if ($result != null) {
            foreach ($result as $key => $value) {
                if ($value['state'] == 0) {
                    $data['pending'] = $value['ccount'];
                }
                if ($value['state'] == 1) {
                    $data['delete_req'] = $value['ccount'];
                }
                if ($value['state'] == 2) {
                    $data['approved'] = $value['ccount'];
                }
                if ($value['state'] == 3) {
                    $data['declined'] = $value['ccount'];
                }
                if ($value['state'] == 4) {
                    $data['sent'] = $value['ccount'];
                }
            }
        }
        return array(
            'title' => "Create New Userx",
            'title_descr' => "Create New Agent",
            'reg_data' => $data,
        );
    }



    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/help", name="mHelp")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     */
    public function agentHelpAction()
    {
        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");
        return array(
            'title' => "Create New Userx",
            'title_descr' => "Create New Agent",
            'reg_data' => $data,
        );
    }
}
