<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Anto;
use aimgroup\DashboardBundle\Form\AntoType;

use aimgroup\RestApiBundle\Entity\User;
/**
 * Anto controller.
 *
 * @Route("/anto")
 */
class AntoController extends Controller {

    
    
    /**
     * Displays a form to edit an existing Anto entity.
     *
     * @Route("/antoo/{access_token}/{msisdn}/{account_type}/edit", name="anto_edit")
     * @Method("GET")
     * @Template("DashboardBundle:Anto:add_account_view.html.twig")
     */
    public function antooAction($access_token, $msisdn, $account_type)
    {
        
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Anto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'title' => "sdfds",
            'title_descr' => "sdfds",
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
        
        return array(
            'title' => "sdfds",
            'title_descr' => "sdfds",
            'access_token' => $access_token,
            'msisdn' => $msisdn,
            'account_type' => $account_type
        );
    }
    
    
    
    
    
    
    
    
    /**
     * Lists all Anto entities.
     *
     * @Route("/", name="anto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:Anto')->findAll();

        return array(
            'title' => "sdfds",
            'title_descr' => "sdfds",
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Anto entity.
     *
     * @Route("/", name="anto_create")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Anto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anto_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Anto entity.
     *
     * @param Anto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Anto $entity) {
        $form = $this->createForm(new AntoType(), $entity, array(
            'action' => $this->generateUrl('anto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Anto entity.
     *
     * @Route("/new", name="anto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Anto();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'title' => "sdfds",
            'title_descr' => "sdfds",
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Anto entity.
     *
     * @Route("/{id}", name="anto_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Anto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'title' => "sdfds",
            'title_descr' => "sdfds",
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Anto entity.
     *
     * @Route("/{id}/edit", name="anto_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Anto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'title' => "sdfds",
            'title_descr' => "sdfds",
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Anto entity.
     *
     * @param Anto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Anto $entity) {
        $form = $this->createForm(new AntoType(), $entity, array(
            'action' => $this->generateUrl('anto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Anto entity.
     *
     * @Route("/{id}", name="anto_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Anto:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Anto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Anto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anto_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Anto entity.
     *
     * @Route("/{id}", name="anto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:Anto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Anto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anto'));
    }

    /**
     * Creates a form to delete a Anto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('anto_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }
    
    

}
