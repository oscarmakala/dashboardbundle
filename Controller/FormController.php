<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\Entity\ConfigMaster;
use aimgroup\DashboardBundle\Entity\FieldNames;
use aimgroup\DashboardBundle\Entity\FieldTypes;
use aimgroup\DashboardBundle\Entity\Region;
use aimgroup\DashboardBundle\Entity\Step;
use aimgroup\DashboardBundle\Entity\StepField;
use aimgroup\DashboardBundle\Entity\Territory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * This FormController is used to manage configuration which are supposed to be sent to mobile devices.
 *
 * @author Oscar Makala
 *
 * @Route("admin/forms")
 */
class FormController extends AbstractController
{

    var $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     *
     * @Route("/", name="admin/forms")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @param Request $request
     * @Route("/lists",name="lists")
     * @Method({"POST","GET"})
     */
    public function listStepAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $steps = $em->createQuery("SELECT s.id, s.title,s.next,concat('step',s.id) as step FROM DashboardBundle:Step s")
                ->getResult();
            $resp->setRecords($steps);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/listStepField",name="listStepField")
     * @Method({"POST","GET"})
     *
     */
    public function listStepFieldAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getEntityManager();
            $attributes = $request->query->all();
            $fields = $em->createQuery("SELECT f.id as fieldId,f.skey,f.required,f.regex,f.hint,f.type,f.regex,f.regexError,f.maxLength,f.maxLengthError,f.action
                    FROM DashboardBundle:StepField f WHERE f.step =:step")
                ->setParameter('step', $attributes["stepId"])
                ->getResult();
            $resp->setRecords($fields);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/listFieldKeys",name="listFieldKeys")
     * @Method({"POST","GET"})
     */
    public function listFieldKeysAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $fieldNames = $em->createQuery("SELECT f.id as Value,f.name as DisplayText FROM DashboardBundle:FieldNames f")
                ->getResult();
            $resp->setOptions($fieldNames);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/listFieldTypes",name="listFieldTypes")
     * @Method({"POST","GET"})
     */
    public function listFieldKeysTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $fieldNames = $em->createQuery("SELECT f.id as Value,f.name as DisplayText FROM DashboardBundle:FieldTypes f")
                ->getResult();
            $resp->setOptions($fieldNames);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/createStep",name="createStep")
     * @Method({"POST","GET"})
     */
    public function createStepAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $step = new Step();
            $step->setNext($attributes["next"]);
            $step->setTitle($attributes["title"]);
            $em->persist($step);
            $em->flush();
            $resp->setRecord(array('id' => $step->getId(), 'title' => $step->getTitle(), 'next' => $step->getNext()));
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/createStepField",name="createStepField")
     * @Method({"POST","GET"})
     */
    public function createStepFieldAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            $step = $em->getRepository('DashboardBundle:Step')->findOneBy(array('id' => $attributes['id']));
            if ($step) {
                $field = new StepField();

                $field->setSkey($attributes['skey']);
                $field->setHint($attributes['hint']);
                $field->setRequired($attributes['required']);
                $field->setType($attributes['type']);


                if (isset($attributes['action'])) {
                    $field->setAction($attributes['action']);
                }
                if (isset($attributes["regex"])) {
                    $field->setRegex($attributes['regex']);
                }
                if (isset($attributes["regexError"])) {
                    $field->setRegexError($attributes['regexError']);
                }
                if (isset($attributes["maxLength"])) {
                    $field->setMaxLength($attributes['maxLength']);
                }
                if (isset($attributes["maxLengthError"])) {
                    $field->setMaxLengthError($attributes['maxLengthError']);
                }

                $field->setStep($step);
                $em->persist($field);
                $em->flush();

                $resp->setRecord(array(
                        "fieldId" => $field->getId(),
                        "hint" => $field->getHint(),
                        "skey" => $field->getSkey(),
                        "required" => $field->getRequired(),
                        "type" => $field->getType(),
                        "regex" => $field->getRegex(),
                        "regexError" => $field->getRegexError(),
                        "maxLength" => $field->getMaxLength(),
                        "maxLengthError" => $field->getMaxLengthError()
                    )
                );
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/updateStepField",name="updateStepField")
     * @Method({"POST","GET"})
     */
    public function updateStepFieldAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $stepField = $em->getRepository('DashboardBundle:StepField')->findOneBy(array('id' => $attributes['fieldId']));
            if ($stepField) {

                $stepField->setSkey($attributes['skey']);
                $stepField->setHint($attributes['hint']);
                $stepField->setRequired($attributes['required']);
                $stepField->setType($attributes['type']);


                if (isset($attributes['action'])) {
                    $stepField->setAction($attributes['action']);
                }
                if (isset($attributes["regex"])) {
                    $stepField->setRegex($attributes['regex']);
                }
                if (isset($attributes["regexError"])) {
                    $stepField->setRegexError($attributes['regexError']);
                }

                if (isset($attributes["maxLength"])) {
                    $stepField->setMaxLength($attributes['maxLength']);
                }
                if (isset($attributes["maxLengthError"])) {
                    $stepField->setMaxLengthError($attributes['maxLengthError']);
                }
                if (isset($attributes["action"])) {
                    $stepField->setAction($attributes['action']);
                }


                $em->flush();

                $resp->setRecord(array(
                        "fieldId" => $stepField->getId(),
                        "skey" => $stepField->getSkey(),
                        "required" => $stepField->getRequired(),
                        "type" => $stepField->getType(),
                        "regex" => $stepField->getRegex(),
                        "regexError" => $stepField->getRegexError(),
                        "maxLength" => $stepField->getMaxLength(),
                        "maxLengthError" => $stepField->getMaxLengthError()
                    )
                );
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/updateStep",name="updateStep")
     * @Method({"POST","GET"})
     */
    public function updateStepAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $step = $em->getRepository('DashboardBundle:Step')->findOneBy(array('id' => $attributes['id']));
            if ($step) {
                $step->setNext($attributes["next"]);
                $step->setTitle($attributes["title"]);
                $em->flush();

                $resp->setRecord(array('id' => $step->getId(), 'title' => $step->getTitle(), 'next' => $step->getNext()));
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Load existing Dynamic Form for the mobile App.
     *
     * @Route("/load_form", name="load_form")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:dynamic_form.html.twig")
     */
    public function loadDynamicFormAction(Request $request)
    {
        if (!in_array(29, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }


        $em = $this->getDoctrine()->getManager();
        $dataColumns = $em->getConnection()->getSchemaManager()->listTableColumns('registration');
        return array(
            'title' => "Dynamic Form Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'dataColumns' => $dataColumns
        );
    }

    /**
     *
     * This code is roughly built, find better way to optimize this
     *
     * Save existing Dynamic Form ready for publishing
     * @param Request $request
     * @Route("/save_form",name="save_form")
     * @Method("GET")
     *
     */
    public function saveDynamicFormAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();
            $steps = $em->getRepository("DashboardBundle:Step")->findAll();
            $fields = $em->getRepository("DashboardBundle:StepField")->findAll();
            $fieldNames = $em->getRepository("DashboardBundle:FieldNames")->findAll();
            $regions = $em->getRepository("DashboardBundle:Region")->findAll();


            /** @var  $fieldName FieldNames */
            foreach ($fieldNames as $fieldName) {
                $nameArray[$fieldName->getId()] = $fieldName->getName();
            }
            $fieldTypes = $em->getRepository("DashboardBundle:FieldTypes")->findAll();
            /** @var  $fieldName FieldTypes */
            foreach ($fieldTypes as $fieldType) {
                $typeArray[$fieldType->getId()] = $fieldType->getName();
            }
            if ($steps) {
                /** @var  $step Step */
                foreach ($steps as $step) {

                    $stepName = "step" . $step->getId();
                    $top["next"] = $step->getNext();
                    $top["title"] = $step->getTitle();
                    $top["fields"] = array();
                    /** @var  $field StepField */
                    foreach ($fields as $field) {
                        if ($field->getStep() === $step) {
                            $inner = array();
                            $inner["key"] = $nameArray[$field->getSkey()];
                            //$hint = $field->getHint();
                            $inner["hint"] = $this->generateLanguageArray($field->getHint());
                            $inner["type"] = $typeArray[$field->getType()];
                            if ($field->getRequired()) {
                                $inner["required"] = $field->getRequired();
                            }
                            if (!empty($field->getMaxLength()) && strlen($field->getMaxLengthError()) > 0) {
                                $inner["max_length"] = array();
                                $inner["max_length"]["value"] = $field->getMaxLength();
                                $inner["max_length"]["err"] = $this->generateLanguageArray($field->getMaxLengthError());
                            }
                            if (!empty($field->getRegex())) {
                                $inner["regex"] = array();
                                $inner["regex"]["value"] = $field->getRegex();
                                $inner["regex"]["err"] = $field->getRegexError();
                                $inner["regex"]["err"]=$this->generateLanguageArray($field->getRegexError());
                            } else if (strcmp($inner["key"], "gender") == 0) {
                                $inner["options"] = array();
                                $t["key"] = "Female";
                                $t["value"] = "F";
                                array_push($inner["options"], $t);
                                $t["key"] = "Male";
                                $t["value"] = "M";
                                array_push($inner["options"], $t);
                            } else if (strcmp($inner["key"], "region") == 0) {
                                $inner["options"] = array();
                                $inner["child"] = "territory";
                                $t = array();
                                $t["value"] = "Select Region";
                                $t["key"] = "";
                                array_push($inner["options"], $t);
                                /** @var  $region Region */
                                foreach ($regions as $region) {
                                    $t["value"] = $region->getCode();
                                    $t["key"] = $region->getName();
                                    array_push($inner["options"], $t);
                                }
                            } else if (strcmp($inner["key"], "territory") == 0) {
                                $inner["v_options"] = array();
                                /** @var  $region Region */
                                foreach ($regions as $region) {
                                    $territories = $region->getTerritories();
                                    $r = array();
                                    $s = array();
                                    /** @var  $territory Territory */
                                    foreach ($territories as $territory) {
                                        $v["value"] = $territory->getId();
                                        $v["key"] = $territory->getName();
                                        array_push($r, $v);
                                    }
                                    $s[$region->getCode()] = $r;
                                    array_push($inner["v_options"], $s);
                                }
                            } else if (strcmp($inner["key"], "id-type") == 0) {
                                $inner["options"] = array();
                                $idtypes = $em->getRepository("DashboardBundle:Idtype")
                                    ->createQueryBuilder('e')
                                    ->select('e.id as value,e.name as key')
                                    ->getQuery()
                                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
                                $inner["options"] = $idtypes;
                            }

                            array_push($top["fields"], $inner);
                        }
                    }

                    $json[$stepName] = $top;
                }
            }
//            /**
//             * remove null values
//             */
//            echo "not working";
//            echo "<pre>";
//            print_r($json);
//            echo "</pre>";
//

            $json["count"] = sizeof($json);
            $jsonString = json_encode($json);

            $configMaster = new ConfigMaster();
            $configMaster->setConfig($jsonString);
            $configMaster->setName("forms");
            $configMaster->setCreatedOn(new \DateTime());
            $configMaster->setConfigType("formConfig");


            /** @var  $configuration ConfigMaster */
            $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                array("configType" => "formConfig")
            );
            if (!$configuration) {
                $em->persist($configMaster);
            } else {
                $configuration->setConfig($configMaster->getConfig());
                $configuration->setVersion($configuration->getVersion() + 1);
            }

            $em->flush();
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/display_form",name="display_form")
     * @Method("GET")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function displaySavedDynamicFormAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();

            $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findBy(
                array("configType" => "formConfig")
            );

            $resp->setItem($configurations);
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/publish",name="publishForm")
     */
    public function publishFormAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = $request->request->all();
            $em = $this->getDoctrine()->getManager();

            $deviceId = $attributes["deviceId"];

            $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                array("configType" => "formConfig")
            );
            $configVAlue["form"] = json_decode($configurations->getConfig(), true);
            $configVAlue["type"] = "form_config";
            $configVAlue["operator"] = $this->container->getParameter("operator_id");

            $respond = $this->get("api.helper")->publish($deviceId, $this->container->getParameter("mqtt_formupdate_msg"), json_encode($configVAlue));
            $status = true;
            $resp->setItem($respond);
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    private function generateLanguageArray($value){
        if ($value) {
            $xx = array();
            $xploded = explode(",",$value);
            foreach ($xploded as $xplode) {
                if (strpos($xplode, ':')) {
                    $langText = explode(":", $xplode);
                    $xx[$langText[0]] = $langText[1];
                }
            }
            return $xx;
        } else {
            return array();
        }
    }
}
