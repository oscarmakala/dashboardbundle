<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class RegistrationController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("admin/registration")
 */
class RegistrationController extends Controller
{
    
    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * View Reports
     *
     * @Route("/", name="admin/registration")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registrations.html.twig")
     */
    public function indexAction()
    {
        if(!in_array(2, json_decode($this->session->get('user_role_perms')))){
            return $this->redirect($this->generateUrl('admin'));
        }
        
        
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RestApiBundle:Registration')->findAll();

        return array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report | All registrations",
            'registrations' => $entities,
        );
    }
}
