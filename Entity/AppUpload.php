<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUpload
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\DashboardBundle\Entity\AppUploadRepository")
 * @ORM\HasLifeCycleCallbacks()
 */
class AppUpload {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;

    /**
     * @var string
     *
     * @ORM\Column(name="version_name", type="string", length=255)
     */
    private $versionName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="file_location", type="string", length=255)
     */
    private $fileLocation;

    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id")
     */
    private $owner;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return AppUpload
     * @ORM\PrePersist
     */
    public function setCreatedDate() {
        if (!isset($this->createdDate)) {
            $this->createdDate = new \DateTime();
        }

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * Set versionName
     *
     * @param string $versionName
     * @return AppUpload
     */
    public function setVersionName($versionName) {
        $this->versionName = $versionName;

        return $this;
    }

    /**
     * Get versionName
     *
     * @return string 
     */
    public function getVersionName() {
        return $this->versionName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AppUpload
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set fileLocation
     *
     * @param string $fileLocation
     * @return AppUpload
     */
    public function setFileLocation($fileLocation) {
        $this->fileLocation = $fileLocation;

        return $this;
    }

    /**
     * Get fileLocation
     *
     * @return string 
     */
    public function getFileLocation() {
        return $this->fileLocation;
    }

    /**
     * 
     * @return User
     */
    public function getOwner() {
        return $this->owner;
    }

    /**
     * 
     * @param User $owner
     */
    public function setOwner($owner) {
        $this->owner = $owner;
    }

}
