<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Faq
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\DashboardBundle\Entity\FaqRepository")
 */
class Faq
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="faq_name", type="string", length=255)
     */
    private $faqName;

    /**
     * @var string
     *
     * @ORM\Column(name="faq_content", type="text")
     */
    private $faqContent;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set faqName
     *
     * @param string $faqName
     * @return Faq
     */
    public function setFaqName($faqName)
    {
        $this->faqName = $faqName;

        return $this;
    }

    /**
     * Get faqName
     *
     * @return string 
     */
    public function getFaqName()
    {
        return $this->faqName;
    }

    /**
     * Set faqContent
     *
     * @param string $faqContent
     * @return Faq
     */
    public function setFaqContent($faqContent)
    {
        $this->faqContent = $faqContent;

        return $this;
    }

    /**
     * Get faqContent
     *
     * @return string 
     */
    public function getFaqContent()
    {
        return $this->faqContent;
    }
}
