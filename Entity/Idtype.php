<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Idtype
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\DashboardBundle\Entity\IdtypeRepository")
 */
class Idtype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_count", type="integer")
     */
    private $useCount;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Idtype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set useCount
     *
     * @param integer $useCount
     * @return Idtype
     */
    public function setUseCount($useCount)
    {
        $this->useCount = $useCount;

        return $this;
    }

    /**
     * Get useCount
     *
     * @return integer 
     */
    public function getUseCount()
    {
        return $this->useCount;
    }
}
