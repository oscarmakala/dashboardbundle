<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\DashboardBundle\Entity\Agent;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @author Michael Tarimo
 *
 * @ORM\Entity
 * @ORM\Table(name="super_agent")
 *
 */
class SuperAgent extends User {

    /**
     * @ORM\OneToMany(targetEntity="aimgroup\DashboardBundle\Entity\Agent", mappedBy="superAgent")
     */
    private $agents;

    /**
     * @return mixed
     */
    public function getAgents() {
        return $this->agents;
    }

    /**
     * @param mixed $agents
     */
    public function setAgents($agents) {
        $this->agents = $agents;
    }
}