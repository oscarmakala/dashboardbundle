<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigMaster
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ConfigMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="configType", type="string", length=30)
     */
    private $configType;

    /**
     * @var string
     *
     * @ORM\Column(name="config", type="text")
     */
    private $config;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdOn", type="datetime")
     */
    private $createdOn;


    /**
     * @var integer
     * @ORM\Column(name="version", type="integer")
     */
    private $version=1;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ConfigMaster
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set configType
     *
     * @param string $configType
     * @return ConfigMaster
     */
    public function setConfigType($configType)
    {
        $this->configType = $configType;

        return $this;
    }

    /**
     * Get configType
     *
     * @return string 
     */
    public function getConfigType()
    {
        return $this->configType;
    }

    /**
     * Set config
     *
     * @param string $config
     * @return ConfigMaster
     */
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return string 
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return ConfigMaster
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}
